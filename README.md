 FULL TEXT SEARCH
------------------
------------------

### Author : _Naved Afzal_

### Email : _naved.afzal@mountblue.tech_

## Abstract

*This paper contain an overview about Full text search. Furthermore, it contains information about various types of Full text search and different challenges in implementing it and finally concludes with advantages of Full text search.*

### Keywords :  (_Search engine, Regular expressions, Pattern_)

  
## **INTRODUCTION**
*Full text search is a technique which allow us to search data through documents and databases not only by title but also by content. Rather than only analyzing description of document , Full text search go through entire content of the document giving user more relevant or exact information. This technique was introduce in 1990. Search at time time was very time consuming, Full text search optimized the search process and now it is used in all major platforms which deal with large data. A very common example is Google.*

## **NEED OF FULL TEXT SEARCH**

Full text search is usully used when
* a name of the person in a list or a database
* a word or a phrase in a document
* a web page on the internet
* products in an online store, etc
* a regular expression

## Full text search construction
Full text search can be divided into two groups:
1. String searching algorithm
2. Regular expression algorithm



## String Searching Algorithm
1. Rabin-karp algorithm 
2. Knuth-Morris-Pratt algorithm
3. Boyer-Moore algorithm
4. Approximate matching algorithm

## Regular expression algorithm
1. Indexed search

## Complications in full text search implementation
Some common problems are listed below
### SYNONYM PROBLEM
It is one of the biggest problem of Full text Search. It arises because there are often many ways to refer a particular concept.<br>
Example:: if a searcher seeks information on  leprosy, he would likely enter “leprosy” in the search box and expect complete results. However, some online documents refer to this disease as “Hansen’s disease.” <br>
another variant of synonym problem can be words  that  mean  the  exact  same  thing  can  sometimes be spelled differently, as in variant British and American spellings. In full-text searching,a search for “harbour” will miss results that use the spelling “harbor.”
                     
### HOMONYM PROBLEM
The opposite of synonym problem is homonym problem. It occurs when a singe word has more than one meaning. For  example,  a  search  on “cookies” will pull up documents both about the food and the little files stored on a computer

### NOT ABLE TO SORT
Sorting reduces the difficulty of searching to a very large extent and enhances the quality of information retrieval by arranging search result in a meaningful and predictable order. For example, sorting search  results by publication date (oldest first or most  recent first) is helpful to searchers looking for recent or old publications. Traditional full-text  search engines cannot perform this type of sort because they don’t know the publication dates of the documents in the database.

## Compairing full text search tool in database to full text search engine
Generally Relational databases are really good at storing, refreshing and manipulating data. They provide facility of multiple search for specific values of fields. But, when it comes to relevant displaying of result and processing huge amount of structured data full text search engine have upper hand.<br>
Full text search engine can be configured depending on project which is not possible with relational databases. Full text search engines are open to be enhanced, so one can implement your own stemming algorithm for your needs and put in the engine.<br>
Some of the most popular full text search engines are Apache Solr, Sphinx etc.

## Some poppular full text search Softwares

## Lucene

Lucene is a very popular open source Java based search library. It is a fast search library. It is generally used in Java based applications to add document search capability to an application in a simple and effective way. 

## Solr and Elastic search

### Solr

Solr is an open-source search platform used to build search applications. It was built on top of Lucene . Solr is deployment-ready, fast and hugely scalable. The applications built with Solr are sophisticated and usually deliver high performance. Solr is a scalable, ready to deploy, search engine optimized to search huge chunk of textual data.

Solr can also be used along with Hadoop. Hadoop handles a large amount of data,so Solr can help in finding the required information from such a huge data bundle. Solr can also be used for storage purpose. similar to other NoSQL databases, it is a non-relational data storage and processing platform.

## Features of Solr

* Enterprise ready
* Full text search
* No SQL database
* Admin interface
* Text centric and sorted by relevance

Unlike Lucene, one do not need to have Java programming skills when working with Solr. It provides   ready-to-deploy service to create a search box featuring autocomplete, which Lucene doesn’t provide. with the help of Solr, one can distribute, scale, and manage index for huge scale

## ElasticSearch

Elasticsearch is an open-source search engine built on top of Lucene. It adds Lucene’s powerful searching and indexing and properties using RESTful APIs and also it archives the distribution of data on many different servers using the shards and index concept. Elastic search based on JSON and is mostly suitable for NoSQL data and time series.

This tool is much younger than Solr, but it has gained a lot of popularity because of its feature-rich use cases. Some of its primary features include distributed full-text distributed search, high availability, powerful query DSL, multi tenancy, Geo Search, and horizontal scaling.


## Relative Popularity

Solr and ElasticSearch are competing search servers. Both Slr and ElasticSearch are built on top of Lucene, so most of their basic features are approximately same.

According to DB-Engines(website), use to ranks search engines and database management systems according to their demand and popularity, Elastic search is ranked number one, and Solr is ranked number three.

Solr had acquired popularity in its early years of existence, but Elastic search has become more popular search engine since 2016.

## Data source

Both Solr and eklastic search support a large range of data sources.

Solr uses request handlers to ingest data from XML files, CSV files, databases, Microsoft Word documents, and PDFs .Elasticsearch, on the other hand, is completely JSON-based.


Lucene is a search engine packaged together in a set of jar files. Many of the applications embed the Lucene files directly into their system and then manually create and search their Lucene index with the help of the Lucene APIs.

ElasticSearch and Solr take those Lucene APIs and then add features to them, and make the APIs accessible through an easy to deploy web server 




## REFERENCES
* https://en.wikipedia.org/wiki/Full-text_search
* https://www.techopedia.com/definition/17113/full-text-search
* https://www.youtube.com/watch?v=2OY4tE2TrcI&t=233s
* https://www.youtube.com/watch?v=GeMlUbbZhKQ&t=42s
* http://digital.auraria.edu/IR00000037/00001
